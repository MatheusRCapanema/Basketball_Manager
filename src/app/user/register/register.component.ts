import { Component } from '@angular/core';
import { AuthCommonWrapperComponent } from "../shared/components/auth-common-wrapper/auth-common-wrapper.component";
import { MatCheckbox } from "@angular/material/checkbox";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { RegisterData, UserService } from "../shared/service/user.service";
import { Router } from "@angular/router";
import { take } from "rxjs";

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    AuthCommonWrapperComponent,
    MatCheckbox,
    ReactiveFormsModule
  ],
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent {
  form = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    firstname: ['', Validators.required],
    lastname: ['', Validators.required]
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  submit(): void {
    if(!this.form.valid) {
      return;
    }

    const data: RegisterData = this.form.getRawValue()

    this.userService.register(data)
      .pipe(take(1))
      .subscribe(response => this.router.navigate(["auth"]))
  }
}
