import { Component } from '@angular/core';
import { AuthCommonWrapperComponent } from "../shared/components/auth-common-wrapper/auth-common-wrapper.component";
import { MatCheckbox } from "@angular/material/checkbox";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { UserService } from "../shared/service/user.service";
import { take } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [
    AuthCommonWrapperComponent,
    MatCheckbox,
    ReactiveFormsModule
  ],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss'
})
export class AuthComponent {
  form = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  onRegister(): void {
    this.router.navigate(["register"])
  }

  submit(): void {
    if(!this.form.valid) {
      return;
    }
    const data = this.form.getRawValue()

    this.userService.authenticate({ username: data.username!, password: data.password! })
      .pipe(take(1))
      .subscribe(response => {
        localStorage.setItem("user", response.toString());
        this.router.navigate(["home"])
      })
  }
}
