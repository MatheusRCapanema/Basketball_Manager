import { Component } from '@angular/core';
import { AuthCommonWrapperComponent } from "../shared/components/auth-common-wrapper/auth-common-wrapper.component";

@Component({
  selector: 'app-forgot-password',
  standalone: true,
  imports: [
    AuthCommonWrapperComponent
  ],
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.scss'
})
export class ForgotPasswordComponent {

}
