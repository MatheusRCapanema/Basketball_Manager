import { CanActivateFn, Router } from '@angular/router';
import { inject } from "@angular/core";

export const AuthGuard: CanActivateFn = (route, state) => {
  const router: Router = inject(Router);
  return localStorage.getItem("user") ? true : router.createUrlTree(["auth"])
};
