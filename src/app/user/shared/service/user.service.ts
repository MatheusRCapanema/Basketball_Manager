import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs";

export interface RegisterData {
  email: string | null,
  firstname: string | null,
  lastname: string | null,
  password: string | null,
}

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private http: HttpClient) {}

  authenticate(authData: { username: string, password: string }) {
    return this.http.post("http://127.0.0.1:5000/api/users/signin", authData);
  }

  register(registerData: RegisterData) {
    return this.http.post("http://127.0.0.1:5000/api/users/signup", registerData);
  }
}
