import { Component } from '@angular/core';

@Component({
  selector: 'auth-common-wrapper',
  standalone: true,
  imports: [],
  templateUrl: './auth-common-wrapper.component.html',
  styleUrl: './auth-common-wrapper.component.scss'
})
export class AuthCommonWrapperComponent {

}
