import { Routes } from '@angular/router';
import { AuthGuard } from "./user/shared/guard/auth.guard";

export const routes: Routes = [
  {
    path: 'auth',
    loadComponent: () => import('./user/auth/auth.component').then(cmp => cmp.AuthComponent)
  },
  {
    path: 'register',
    loadComponent: () => import('./user/register/register.component').then(cmp => cmp.RegisterComponent)
  },
  {
    path: 'forgot-password',
    loadComponent: () => import('./user/forgot-password/forgot-password.component').then(cmp => cmp.ForgotPasswordComponent)
  },
  {
    path: 'home',
    canActivate: [AuthGuard],
    loadComponent: () => import('./home/home.component').then(cmp => cmp.HomeComponent)
  }
];
